/* Repurposed test-paned.c for autopkgtest
 * Creates a penel with 3 buttons.
 * Clicking a button causes it to disappear.
 * When no buttons are left, quit normally.
 * Author: Nathan Pratta Teodosio <nathan.teodosio@canonical.com>
 */
#include <libpanel.h>
#include <gtk/gtk.h>
GMainLoop *main_loop;
char *argv0 = NULL;

static void removebutton(GtkButton *b, gpointer user_data) {
  PanelPaned *p = (PanelPaned *)user_data;
  panel_paned_remove(p, GTK_WIDGET(b));
  g_print("%s: A button was removed from the panel.\n", argv0);
  if (panel_paned_get_n_children(p) == 0) {
    g_print("%s: No buttons left. Exiting.\n", argv0);
    g_main_loop_quit(main_loop);
  }
}

int main(int argc, char *argv[]) {
  argv0 = argv[0];
  GtkWidget *window;
  GtkWidget *paned;

  gtk_init();
  panel_init();

  main_loop = g_main_loop_new(NULL, FALSE);
  window = gtk_window_new();
  paned = panel_paned_new();
  gtk_window_set_child(GTK_WINDOW(window), paned);

  for (guint i = 0; i < 3; i++) {
    GtkWidget *button = gtk_button_new();
    panel_paned_append(PANEL_PANED(paned), button);
    g_signal_connect(button, "clicked", G_CALLBACK(removebutton), paned);
  }
  gtk_window_present(GTK_WINDOW(window));
  g_main_loop_run(main_loop);
  return 0;
}
